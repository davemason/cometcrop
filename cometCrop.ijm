// @File(label="Input directory", description="Select the directory with input images", style="directory") inPath
// @Integer(label="Longpass filter on objects to include (pixels)",description="When refining the mask, only include objects bigger than this (pixels)",value=500,persist=false) minObjSize
// @Boolean(label="Exclude on edges?",description="Do you want to exclude comets on the edge of images?",value=true) excludeEdge
// @String(label="Verbose Level", description="Higher number prints more info. 2 and greater disables batch mode (SLOW)", choices={"0","1","2","3"},value="1",persist=false ) verbose

//
// --------------------------------------------------
// Crop Images for comet assay analysis
// --------------------------------------------------
//
//
//
//												
//									- Dave Mason [dnmason@liv.ac.uk] March 2018
//										Centre for Cell Imaging, Liverpool
//
//---------------------------------------------------

//-- RUN OPTIONS:

//------------------------------------
//-- Define verbosity:
//	0= Finished message only
//	1= Print updates on processing to log
//	2= As 2 with images shown (SLOW)
//	3= Full debug output (as 2 but also lists arrays)
verbose=parseInt(verbose);
//verbose=0;
//------------------------------------

if (verbose<2){setBatchMode(true);}

//--------------------------------//------------------------------------
//-- PART 1: Get file list and specify arrays
//--------------------------------//------------------------------------
saveSettings();
if (verbose>0){print("------------------------");}
if (verbose>0){print("Processing "+inPath);}
dirListAll = getFileList(inPath);

if (verbose>0){print("Found "+dirListAll.length+" total files");}
if (verbose>2){Array.print(dirListAll);}
//-- Refine dirList to only include requested filetypes
dirList=newArray("removeMe");
for (q = 0; q < dirListAll.length; q++) {
	if(endsWith(dirListAll[q], "tif")){
			dirList=Array.concat(dirListAll[q],dirList);
			if (verbose>2){print("Including "+dirListAll[q]);}
	}else{
			if (verbose>2){print("Excluding "+dirListAll[q]);}
		}
}


//-- Sanity check, dirList will always have at least one entry
if (dirList.length==1){
	//-- No eligible files found
	if (verbose>0){
		print("[ERROR] No eligible files found. Quitting.");
		exit("No eligible files found!")
				}	
	}else{
	//-- Remove the dummy line, sort and continue
	dirList=Array.trim(dirList,dirList.length-1);
	Array.sort(dirList);
}
if (verbose>0){print("Found "+dirList.length+" eligible files");}
if (verbose>2){Array.print(dirList);}

//--------------------------------//------------------------------------
//-- PART 2: Create output folders
//--------------------------------//------------------------------------
//--2do: check for existance of output folder to prevent overwrites?

//-- Create output for comets
File.makeDirectory(inPath+File.separator+"comets");

//-- Create output for summary
File.makeDirectory(inPath+File.separator+"summary");


//--------------------------------//------------------------------------
//-- PART 3: Loop through file list
//--------------------------------//------------------------------------
for (i=0; i<dirList.length; i++){

open(inPath+File.separator+dirList[i]);

if (verbose>0){
print("------------------------");
print("Processing file ["+(i+1)+"/"+dirList.length+"]: "+inPath+File.separator+dirList[i]);}

title=getTitle();
getDimensions(w, h, c, s, f);

//-- Create a binary mask
run("Duplicate...", "title=mask");
run("Median...", "radius=15");

//-- BG sub to remove uneven illumination and speckles not removed by median filtering
rb=100;
//-- Sliding parabaloid retains edge features so they are correctly included / excluded
run("Subtract Background...", "rolling="+rb+" sliding");

//-- NOTE: Huang or Li work better for higher variance in comet intensities
//-- but will give more false positives for high BG images
th="Li";
setAutoThreshold(th+" dark");

setOption("BlackBackground", true);
run("Convert to Mask");

//-- Refine mask
if (excludeEdge==true){
	run("Analyze Particles...", "size="+minObjSize+"-Infinity pixel show=Masks exclude in_situ");
}else{
	run("Analyze Particles...", "size="+minObjSize+"-Infinity pixel show=Masks in_situ");
}
//-- set a high tolerance to select objects centroids
run("Find Maxima...", "noise=254 output=[Point Selection]");

//-- Error catch here if there are no comets found
if (selectionType()==-1){
	if (verbose>0){print("Found 0 candidate comets in image");}
} else {

//-- pull coordinates
getSelectionCoordinates(x, y);
if (verbose>0){print("Found "+x.length+" candidate comets in image");}
close("mask");

//-- now you have an array with coordinates loop through and duplicate boxes
for (j=1;j<=x.length;j++){
	selectWindow(title);
	run("Specify...", "width=140 height=140 x="+x[j-1]+" y="+y[j-1]+" centered");
	run("Duplicate...", "title=comet_"+IJ.pad(j,2));

	//-- save out comet files here
	saveAs("Tiff", inPath+File.separator+"comets"+File.separator+replace(dirList[i],".tif","_comet_"+IJ.pad(j,2)+".png"));
}

//-- Repeat to create summary image with overlays (if you do this ahead, you will get overlay edges when regions overlap)
for (j=1;j<=x.length;j++){
	selectWindow(title);
	run("Specify...", "width=140 height=140 x="+x[j-1]+" y="+y[j-1]+" centered");
	//-- Go back to original and demark duplicated area
	selectWindow(title);
	Overlay.addSelection("magenta",3);
	run("Select None");
} //-- Comet loop close

} //-- no comets found selection check

//-- save original with overlays
setColor("yellow");
setFont("Monospaced", 18);
drawString("Excluding < "+minObjSize+"px", 5, 20);
drawString(th+" Threshold", 5, 40);
drawString(rb+"px Rolling Ball", 5, 60);
saveAs("PNG", inPath+File.separator+"summary"+File.separator+replace(dirList[i],".tif","_summary.png"));

close("*");
} //-- Close file Loop

if (verbose>0){print("------------------------");print("Done processing "+dirList.length+" files");}


restoreSettings(); //-- restore original settings

setBatchMode(false);
print("------------------------------------------------");
print("Finished ("+dirList.length+" files processed)");
print("------------------------------------------------");