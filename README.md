# Comet Crop #
This IJ1 script pre-processes tif files for processing with [Open Comet](http://www.cometbio.org/) by finding comets in the image, cropping and resaving them. A summary image is also created showing which comets have been processed.

Written and Tested in [Fiji](http://fiji.sc) 64bit.

### Usage ###

* Download and update [Fiji](http://fiji.sc)
* Open the script in the Fiji Script Editor and hit run. You will be asked for an input folder and several parameters (check the tool tips for more information).
* Hit OK and the script will run. Depending upon the verbose level ("1" is a good place to start) updates will be written out to the log.
* Two folders will be created in the source directory: "comets" contains the individual comets, labelled with the base name and a zero padded number saved as TIF files. "summary" contains PNG images overlaid with the crop regions and some information on processing.

![Example Output](https://bytebucket.org/davemason/cometcrop/raw/e0cf44dff4dfa24ce3e7d29a0567ef3bc5252c31/Summary.png)

### Acknowledgments ###

* The code was written by [Dave Mason](http://pcwww.liv.ac.uk/~dnmason), from the University of Liverpool [Centre for Cell Imaging](http://cci.liv.ac.uk)